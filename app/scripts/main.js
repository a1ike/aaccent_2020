$(document).ready(function () {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('input,textarea')
    .focus(function () {
      $(this)
        .data('placeholder', $(this).attr('placeholder'))
        .attr('placeholder', '');
    })
    .blur(function () {
      $(this).attr('placeholder', $(this).data('placeholder'));
    });

  $('.a-home-work__checkbox').on('click', function (e) {
    e.preventDefault();

    $(this).children('input').prop('checked', true);
  });

  $('.a-home-results-wrap__checkbox').on('click', function () {
    let currentCity = $(this).children('input').val();
    $('.a-header__city').children('span').html(currentCity);
  });

  $('.a-home-results-wrap__checkbox').on('click', function (e) {
    e.preventDefault();

    $(this).children('input').prop('checked', true);
  });

  $('.a-header__toggle').on('click', function (e) {
    e.preventDefault();

    $(this).toggleClass('a-header__toggle_active');
    $('.a-full').slideToggle('fast');
  });

  $('.a-home-services-collapse__header').on('click', function (e) {
    e.preventDefault();

    $(this).toggleClass('a-home-services-collapse__header_active');
    $(this).next().slideToggle('fast');
  });

  $('.a-cases__sort').on('click', function (e) {
    e.preventDefault();

    $(this).next().slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.a-modal').toggle();
  });

  $('.a-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'a-modal__centered') {
      $('.a-modal').hide();
    }
  });

  $('.a-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.a-modal').hide();
  });

  $('.open-thx').on('click', function (e) {
    e.preventDefault();

    $('.a-thx').toggle();
  });

  $('.a-thx__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'a-thx__centered') {
      $('.a-thx').hide();
    }
  });

  $('.a-thx__close').on('click', function (e) {
    e.preventDefault();
    $('.a-thx').hide();
  });

  $('.open-choose').on('click', function (e) {
    e.preventDefault();

    $('.a-choose').toggle();
  });

  $('.a-choose__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'a-choose__centered') {
      $('.a-choose').hide();
    }
  });

  $('.a-choose__close').on('click', function (e) {
    e.preventDefault();
    $('.a-choose').hide();
  });

  $('.a-home-reviews-card').on('click', function (e) {
    e.preventDefault();

    $('.a-review').toggle();
  });

  $('.a-review__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'a-review__centered') {
      $('.a-review').hide();
    }
  });

  $('.a-review__close').on('click', function (e) {
    e.preventDefault();
    $('.a-review').hide();
  });

  $('.a-read__toggle').on('click', function (e) {
    e.preventDefault();

    if ($(this).hasClass('a-read__toggle_active')) {
      $(this).removeClass('a-read__toggle_active');
      $(this).children('span').html('Читать дальше');
      $(this).prev().removeClass('a-read__content_active');

      return;
    }

    $(this).addClass('a-read__toggle_active');
    $(this).children('span').html('Скрыть');
    $(this).prev().addClass('a-read__content_active');
  });

  new Swiper('.a-home-reviews__cards', {
    centeredSlides: true,
    loop: true,
    spaceBetween: 42,
    slidesPerView: 'auto',
    breakpoints: {
      1024: {
        loop: true,
        spaceBetween: 42,
        slidesPerView: 2,
      },
      1200: {
        loop: true,
        spaceBetween: 42,
        slidesPerView: 3,
      },
    },
  });

  new Swiper('.a-another__cards', {
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    navigation: {
      nextEl: '.a-another__next',
      prevEl: '.a-another__prev',
    },
    breakpoints: {
      1024: {
        loop: true,
        spaceBetween: 30,
        slidesPerView: 2,
      },
    },
  });

  new Swiper('.a-info__cards', {
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    navigation: {
      nextEl: '.a-info .swiper-button-next',
      prevEl: '.a-info .swiper-button-prev',
    },
    breakpoints: {
      1024: {
        loop: true,
        spaceBetween: 30,
        slidesPerView: 3,
      },
      1200: {
        loop: true,
        spaceBetween: 30,
        slidesPerView: 5,
      },
    },
  });

  $(function () {
    new WOW().init();
  });

  $(window).on('load', function () {
    if ($(window).width() < 1200) {
      $('.a-full__link').on('click', function (e) {
        e.preventDefault();

        $(this).toggleClass('a-full__link_active');
        $(this).next().slideToggle('fast');
      });

      $('.a-full__contacts').on('click', function (e) {
        e.preventDefault();

        $(this).toggleClass('a-full__contacts_active');
        $(this).next().slideToggle('fast');
      });

      $('.a-footer__link').on('click', function (e) {
        e.preventDefault();

        $(this).toggleClass('a-footer__link_active');
        $(this).next().slideToggle('fast');
      });

      $('.a-home-results-wrap__title').on('click', function (e) {
        e.preventDefault();

        if ($(this).hasClass('a-home-results-wrap__title_active')) {
          $(this).next().slideToggle('fast');
          $(this).removeClass('a-home-results-wrap__title_active');
          return;
        }

        $('.a-home-results-wrap__title').removeClass(
          'a-home-results-wrap__title_active'
        );
        $('.a-home-results_mob').hide();
        $(this).addClass('a-home-results-wrap__title_active');
        $(this).next().slideToggle('fast');
      });

      $('.a-footer__what').appendTo('.a-footer > .container');

      $('.a-footer__policy').appendTo('.a-footer > .container');

      new Swiper('.a-home-specs__cards', {
        centeredSlides: true,
        loop: true,
        spaceBetween: 0,
        slidesPerView: 'auto',
      });

      $('.a-home-reviews__button').appendTo('.a-home-reviews__cards');

      $('#about_replace').detach().insertAfter('#about_replace_to_2');

      $('#about_replace_2').detach().insertAfter('#about_replace_to');
    }
  });

  let integerDivision = function (a, b) {
    return (a - (a % b)) / b;
  };

  let Triangles = {
    CELL_WIDTH: 36.5,
    CELL_HEIGHT: 36.5,
    triangles: {},
    lastTriangle: '',
    previousCoords: null,
    busy: false,
    colors: [
      '#BE1E2D',
      '#ED1C24',
      '#F15A29',
      '#F7941E',
      '#FBB040',
      '#FFF200',
      '#D7DF23',
      '#8DC63F',
      '#00A651',
      '#006838',
      '#00A79D',
      '#27AAE1',
      '#1C75BC',
      '#2B3990',
      '#262262',
      '#662D91',
      '#92278F',
      '#9E1F63',
      '#DA1C5C',
      '#EE2A7B',
    ],
    currentColor: 0,
    init: function () {
      var self = this;
      this.container = $('#triangles');
      if (1) {
        this.halfCellWidth = integerDivision(this.CELL_WIDTH, 2);
        this.halfCellHeight = integerDivision(this.CELL_HEIGHT, 2);
        this.paper = Raphael(this.container.get(0));
        $(window)
          .resize(function () {
            self.paper.setSize(self.container.width(), self.container.height());
          })
          .trigger('resize');
        var mousemove = function (e) {
          var pos = self.container.offset();
          self.mousemove({
            x: e.pageX - pos.left,
            y: e.pageY - pos.top,
          });
        };
        this.container.parent().find('*').mousemove(mousemove);
      }
    },
    mousemove: function (coords, mode2) {
      if (!mode2 && this.busy) return;
      if (!mode2) this.busy = true;
      if (!mode2 && this.previousCoords !== null) {
        var ax = Math.abs(this.previousCoords.x - coords.x);
        var ay = Math.abs(this.previousCoords.y - coords.y);
        if (ax > 1 || ax > 1) {
          var i, k;
          var directionX = this.previousCoords.x < coords.x ? 1 : -1;
          var directionY = this.previousCoords.y < coords.y ? 1 : -1;
          if (ax > ay) {
            for (
              k = 0, i = this.previousCoords.x;
              directionX == 1 ? i <= coords.x : i >= coords.x;
              k++, i += directionX
            ) {
              this.mousemove(
                {
                  x: i,
                  y:
                    this.previousCoords.y +
                    Math.floor((ay * k) / ax) * directionY,
                },
                true
              );
            }
          } else {
            for (
              k = 0, i = this.previousCoords.y;
              directionY == 1 ? i <= coords.y : i >= coords.y;
              k++, i += directionY
            ) {
              this.mousemove(
                {
                  x:
                    this.previousCoords.x +
                    Math.floor((ax * k) / ay) * directionX,
                  y: i,
                },
                true
              );
            }
          }
        }
      }
      var x = coords.x;
      var y = coords.y;
      var col = integerDivision(x, this.CELL_WIDTH);
      var row = integerDivision(y, this.CELL_HEIGHT);
      var cellX = col * this.CELL_WIDTH;
      var cellY = row * this.CELL_HEIGHT;
      var rx = x - cellX;
      var ry = y - cellY;
      var triangle =
        rx < this.halfCellWidth
          ? ry < this.halfCellHeight
            ? rx > ry
              ? 0
              : 3
            : rx + ry > this.CELL_WIDTH
            ? 2
            : 3
          : ry < this.halfCellHeight
          ? rx + ry > this.CELL_WIDTH
            ? 1
            : 0
          : rx > ry
          ? 1
          : 2;
      this.triangle(
        triangle == 0 || triangle == 2 || triangle == 3
          ? cellX
          : cellX + this.halfCellWidth,
        triangle == 0 || triangle == 3
          ? cellY
          : triangle == 1
          ? cellY + this.halfCellHeight
          : cellY + this.CELL_HEIGHT,
        triangle == 0 || triangle == 1
          ? cellX + this.CELL_WIDTH
          : cellX + this.halfCellWidth,
        triangle == 0 || triangle == 1 ? cellY : cellY + this.halfCellHeight,
        triangle == 3
          ? cellX
          : triangle == 0
          ? cellX + this.halfCellWidth
          : cellX + this.CELL_WIDTH,
        triangle == 0 ? cellY + this.halfCellHeight : cellY + this.CELL_HEIGHT
      );
      if (!mode2) {
        this.busy = false;
        this.previousCoords = coords;
      }
    },
    triangle: function (x0, y0, x1, y1, x2, y2) {
      var pathString =
        'M' +
        x0 +
        ' ' +
        y0 +
        'L' +
        x1 +
        ' ' +
        y1 +
        'L' +
        x2 +
        ' ' +
        y2 +
        'L' +
        x0 +
        ' ' +
        y0;
      if (!(pathString in this.triangles))
        this.triangles[pathString] = this.paper.path(pathString);
      if (this.lastTriangle != pathString) {
        var el = this.triangles[pathString];
        el.attr({
          fill: '#ffffff',
          'stroke-width': 0,
        }).animate(
          {
            fill: this.nextColor(),
          },
          200
        );
      }
      this.lastTriangle = pathString;
    },
    nextColor: function () {
      var result = this.colors[this.currentColor];
      if (this.currentColor == this.colors.length - 1) this.currentColor = 0;
      else this.currentColor++;
      return result;
    },
  };

  Triangles.init();
});

$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};
